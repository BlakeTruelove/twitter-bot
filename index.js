console.log("SNES Soundtracks booting up");

//making sure npm run develop works
if (process.env.NODE_ENV === "develop") {
    require("dotenv").config();
};

//rules for node-schedule
var schedule = require("node-schedule");
var rule = new schedule.RecurrenceRule();
  rule.dayOfWeek = 1,
  rule.hour = 10;
  rule.tz = "Etc/GMT+4";

//array to pull soundtracks from
var soundtrackArray = [ "an array of youtube video URLs"];
var soundtrackArrayLength = soundtrackArray.length;

// Create a Twitter object to connect to Twitter API
var Twit = require('twit');

// Pulling keys from another file
var config = require('./config.js');
// Making a Twit object for connection to the API
var T = new Twit(config);

// Setting up a user stream
var stream = T.stream('statuses/filter', { track: '@SnesSoundtracks' });

// Now looking for tweet events
// See: https://dev.Twitter.com/streaming/userstreams
stream.on('tweet', pressStart);

function pressStart(tweet) {

    var soundtrackArrayElement = Math.floor(Math.random() * soundtrackArrayLength);
    var id = tweet.id_str;
    var text = tweet.text;
    var name = tweet.user.screen_name;

    let regex = /(please)/gi;


    let playerOne = text.match(regex) || [];
    let playerTwo = playerOne.length > 0;

    //this helps with errors, but isn't really best practice. It's ok, we're new. This let lets you see if the regex matched and if playerTwo is true or false
    console.log(playerOne);
    console.log(playerTwo);


    // checks text of tweet for mention of SNESSoundtracks
    if (text.includes('@SnesSoundtracks') && playerTwo === true) {

        // Start a reply back to the sender
        var replyText = ("@" + name + " Here's your soundtrack!" + soundtrackArray[soundtrackArrayElement]);

        // Post that tweet
        T.post('statuses/update', { status: replyText, in_reply_to_status_id: id }, gameOver);

    } else {
        console.log("uh-uh-uh, they didn't say the magic word.");
    };

    function gameOver(err, reply) {
        if (err) {
            console.log(err.message);
            console.log("Game Over");
        } else {
            console.log('Tweeted: ' + reply.text);
        }
    };
}

function pressSelect() {

    var soundtrackArrayElement = Math.floor(Math.random() * soundtrackArrayLength);
    var weeklyReplyText = soundtrackArray[soundtrackArrayElement] + " Here's your soundtrack for the week!";
    T.post('statuses/update', { status: weeklyReplyText }, gameOver2);

    function gameOver2(err, reply) {
        if (err) {
            console.log(err.message);
            console.log("Game Over");
        } else {
            console.log('Tweeted: ' + reply.text);
        }
    }
}

 //const job1 = schedule.scheduleJob(rule, pressSelect);

 //job1.on("Every Day Tweet", pressSelect);